from PIL import Image
from optparse import OptionParser

import requests
import urllib
import os
import shutil
import errno
import random
import sys
import threading



class SatnogsOrganizer():
    """A class that organizes a local database of SatNOGS observations"""

    def __init__(
            self, db_dir):
        self.api_root = 'https://network.satnogs.org/api/observations/'
        self.db_dir = db_dir

    def _get_endpoint_observations(self, args):
        return 'https://network.satnogs.org/api/observations/' + args
    
    def _get_endpoint_transmitters(self, args):
        return 'https://network.satnogs.org/api/transmitters/' + args

    def _get_observations(self, observation_id):
        args = '?id=' + str(observation_id)
        return requests.get(self._get_endpoint_observations(args))
    
    def _get_transmitter(self, transmitter_uuid):
        args = '?uuid=' + str(transmitter_uuid)
        return requests.get(self._get_endpoint_transmitters(args))
    
    def organize_vetted_status(self):
        for fname in os.listdir(self.db_dir):
            if fname.endswith(".dat"):
                fname_prefix = fname.split(".", 2)[0]
                obs_id = fname_prefix.split("_",2)[1]
                resp = self._get_observations(obs_id)
                if resp.status_code == 200:
                    data = resp.json()
                    if data:
                        if data[0]["vetted_status"] is not None:
                            path = self.db_dir + '/vetted_status/' + data[0]["vetted_status"]
                            try:
                                os.makedirs(path)
                            except OSError as e:
                                if e.errno != errno.EEXIST:
                                    raise
                            shutil.copy(self.db_dir + "/" + fname, 
                                      path + "/" + os.path.basename(fname))
                elif resp.status_code == 404:
                    finished = True
    
    def organize_transmission_mode(self):
        for fname in os.listdir(self.db_dir):
            if fname.endswith(".dat"):
                fname_prefix = fname.split(".", 2)[0]
                obs_id = fname_prefix.split("_",2)[1]
                resp = self._get_observations(obs_id)
                if resp.status_code == 200:
                    data = resp.json()
                    if data:
                        if data[0]["transmitter"] is not None:
                            resp = self._get_transmitter(data[0]["transmitter"])
                            if resp.status_code == 200:
                                data = resp.json()
                                path = self.db_dir + '/transmission_mode/' + data[0]["mode"]
                                try:
                                    os.makedirs(path)
                                except OSError as e:
                                    if e.errno != errno.EEXIST:
                                        raise
                                shutil.copy(self.db_dir + "/" + fname, 
                                          path + "/" + os.path.basename(fname))
                            elif resp.status_code == 404:
                                finished = True
                elif resp.status_code == 404:
                    finished = True
            
def argument_parser():
    description = 'A tool to facilitate the organization of a local SatNOGS DB.'
    parser = OptionParser(usage="%prog: [options]", description=description)
    parser.add_option(
        "-d", "--database-directory", dest="db_dir", type="string", default='',
        help="Set the path to the database directory [default=%default]")
    return parser

def main(organizer=SatnogsOrganizer, options=None):
    if options is None:
        options, _ = argument_parser().parse_args()

    satnogs_org = organizer(db_dir=options.db_dir)

    satnogs_org.organize_transmission_mode()


if __name__ == '__main__':
    main()
