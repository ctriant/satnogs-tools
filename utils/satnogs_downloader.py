from PIL import Image
from optparse import OptionParser
from tqdm import tqdm

import requests
import urllib
import os
import errno
import random
import sys
import threading



class SatnogsDownloader():
    """A class to facilitate downloadinf SatNOGS data"""

    def __init__(
            self, observation_id, ground_station, satellite_norad_cat_id,
            obs_start, obs_end, transmitter, vetted_status_list, vetted_user,
            dest, obs_num):
        self.root = 'https://network.satnogs.org/api/observations/'
        self.observation_id = observation_id
        self.ground_station = ground_station
        self.satellite_norad_cat_id = satellite_norad_cat_id
        self.obs_start = obs_start
        self.obs_end = obs_end
        self.transmitter = transmitter
        self.vetted_status_list = vetted_status_list
        self.vetted_user = vetted_user
        self.dest = dest
        self.obs_num = obs_num

    def _set_vetted_status(self, status):
        self.vetted_status = status

    def _get_endpoint(self, args):
        return 'https://network.satnogs.org/api/observations/' + args

    def _get_observations(
            self, page):
        args = '?page=' + str(page)
        args += '&end=' + str(self.obs_end)
        args += '&start=' + str(self.obs_start)
        args += '&ground_station=' + str(self.ground_station)
        args += '&satellite__norad_cat_id=' + str(self.satellite_norad_cat_id)
        args += '&id=' + str(self.observation_id)
        args += '&transmitter=' + str(self.transmitter)
        # TODO: Consider vetter_status parameter to optimize query
        args += '&vetted_status=' + str(self.vetted_status)
        args += '&vetted_user=' + str(self.vetted_user)
        return requests.get(self._get_endpoint(args))

    def crop(self, image_path, coords, saved_location):
        """
        @param image_path: The path to the image to edit
        @param coords: A tuple of x/y coordinates (x1, y1, x2, y2)
        @param saved_location: Path to save the cropped image
        """
        image_obj = Image.open(image_path)
        cropped_image = image_obj.crop(coords)
        cropped_image.save(saved_location)

    def download_waterfall_plot(self):

        for s in self.vetted_status_list:
            self._set_vetted_status(s)
            p = 1

            pbar = tqdm(total=int(self.obs_num))
            finished = False
            cnt = 0
            while (cnt < int(self.obs_num) or finished):

                resp = self._get_observations(p)
                if resp.status_code == 200:
                    data = resp.json()
                elif resp.status_code == 404:
                    finished = True
                
                for w in range(1, min(len(data),int(self.obs_num)-cnt+1)):
                    if data[w]["waterfall"] is None:
                        continue

                    if data[w]["vetted_status"] in self.vetted_status:
                        path = self.dest + '/' + data[w]["vetted_status"]
                        try:
                            os.makedirs(path)
                        except OSError as e:
                            if e.errno != errno.EEXIST:
                                raise
                        urllib.urlretrieve(
                            data[w]["waterfall"], path + '/tmp.png')
                        files = next(os.walk(path))[2]
                        self.crop(path + '/tmp.png', (106, 26, 650, 1524),
                                path + '/' + data[w]["vetted_status"] + '_' + str(cnt) + '.png')
                        cnt += 1
                        pbar.update()
                        os.remove(path + '/tmp.png')
                p = p + 1
            pbar.close()


def parse_vetted_status(option, opt, value, parser):
    setattr(parser.values, option.dest, value.split(','))


def argument_parser():
    description = 'A tool to facilitate the downloading of SatNOGS data.'
    parser = OptionParser(usage="%prog: [options]", description=description)
    parser.add_option(
        "-g", "--ground-station", dest="ground_station", type="string", default='',
        help="Set ground-station [default=%default]")
    parser.add_option(
        "-i", "--id", dest="observation_id", type="string", default='',
        help="Set observation id [default=%default]")
    parser.add_option(
        "-c", "--satellite-norad-cat-id", dest="satellite_norad_cat_id", type="string", default='',
        help="Set NORAD category ID[default=%default]")
    parser.add_option(
        "-s", "--observation-start", dest="obs_start", type="string", default='',
        help="Set observation start [default=%default]")
    parser.add_option(
        "-e", "--observation-end", dest="obs_end", type="string", default='',
        help="Set observation end [default=%default]")
    parser.add_option(
        "-t", "--transmitter", dest="transmitter", type="string", default='',
        help="Set satellite transmitter [default=%default]")
    parser.add_option(
        "-v", "--vetted-status", dest="vetted_status_list", type="string", default='good,bad,unknown,failed',
        action='callback', callback=parse_vetted_status,
        help="Set the vetted status of the observation [default=%default]")
    parser.add_option(
        "-u", "--vetted-user", dest="vetted_user", type="string", default='',
        help="Set the vetter of the observation [default=%default]")
    parser.add_option(
        "-d", "--destination", dest="dest", type="string", default='',
        help="Set the destination directory for downloaded waterfalls [default=%default]")
    parser.add_option(
        "-n", "--observation-num", dest="obs_num", type="string", default='',
        help="Set the number of observations per vetting status to download [default=%default]")
    return parser


def main(downloader=SatnogsDownloader, options=None):
    if options is None:
        options, _ = argument_parser().parse_args()

    d = downloader(
        observation_id=options.observation_id, ground_station=options.ground_station,
        satellite_norad_cat_id=options.satellite_norad_cat_id, obs_start=options.obs_start,
        obs_end=options.obs_end, transmitter=options.transmitter, vetted_status_list=options.vetted_status_list,
        vetted_user=options.vetted_user, dest=options.dest, obs_num=options.obs_num)

    d.download_waterfall_plot()


if __name__ == '__main__':
    main()
